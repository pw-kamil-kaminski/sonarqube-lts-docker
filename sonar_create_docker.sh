docker volume create --name sonarqube_data
docker volume create --name sonarqube_extensions
docker volume create --name sonarqube_logs
docker volume create --name sonarqube_conf
docker volume create --name sonarqube_lib_common



docker network create mynet
docker run --name sonar-postgres \
-e POSTGRES_USER=sonar \
-e POSTGRES_PASSWORD=sonar \
-d -p 5432:5432 \
--net mynet \
--restart always \
postgres



docker run --name sonarqube \
-p 9000:9000 \
-m 3g \
-v sonarqube_conf:/opt/sonarqube/conf \
-v sonarqube_extensions:/opt/sonarqube/extensions \
-v sonarqube_logs:/opt/sonarqube/logs \
-v sonarqube_data:/opt/sonarqube/data \
-v sonarqube_lib_common:/opt/sonarqube/lib/common \
-e SONARQUBE_JDBC_URL=jdbc:postgresql://sonar-postgres:5432/sonar \
-e SONARQUBE_JDBC_USERNAME=sonar \
-e SONARQUBE_JDBC_PASSWORD=sonar \
-d --net mynet \
--restart always \
sonarqube:lts
